<?php if (!defined('FLUX_ROOT')) exit; ?>
	<div class="col-md-12">		
		<h2><?php echo htmlspecialchars(Flux::message('ResetPassTitle')) ?></h2>
		<?php if (!empty($errorMessage)): ?>
		<p class="red"><?php echo htmlspecialchars($errorMessage) ?></p>
		<?php endif ?>
		<div class='info'>
			<p><?php echo htmlspecialchars(Flux::message('ResetPassInfo')) ?></p>
			<p><?php echo htmlspecialchars(Flux::message('ResetPassInfo2')) ?></p>
		</div>
		<form action="<?php echo $this->urlWithQs ?>" method="post" class="generic-form">
			<table class="table">
				<div class="col-md-7" >
					<label for="userid"><?php echo htmlspecialchars(Flux::message('ResetPassAccountLabel')) ?></label>
					<input class="form-control" type="text" name="userid" id="userid" />
					<p><?php echo htmlspecialchars(Flux::message('ResetPassAccountInfo')) ?></p>
				</div>
				<div class="col-md-7" >
					<label for="email"><?php echo htmlspecialchars(Flux::message('ResetPassEmailLabel')) ?></label>
					<input class="form-control" type="text" name="email" id="email" />
					<p><?php echo htmlspecialchars(Flux::message('ResetPassEmailInfo')) ?></p>
				</div>
				<div class="col-md-7" >
					<input class="form-btn" type="submit" value="<?php echo htmlspecialchars(Flux::message('ResetPassButton')) ?>" />
				</div>
			</table>
		</form>
	</div>