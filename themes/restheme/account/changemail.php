<?php if (!defined('FLUX_ROOT')) exit; ?>
<div class="col-md-12">		
	<h2><?php echo htmlspecialchars(Flux::message('EmailChangeHeading')) ?></h2>
	<?php if (!empty($errorMessage)): ?>
	<p class="red"><?php echo htmlspecialchars($errorMessage) ?></p>
	<?php endif ?>
	<div class='info'>
		<p><?php echo htmlspecialchars(Flux::message('EmailChangeInfo')) ?></p>
		<?php if (Flux::config('RequireChangeConfirm')): ?>
		<p><?php echo htmlspecialchars(Flux::message('EmailChangeInfo2')) ?></p>
		<?php endif ?>
	</div>
	<form action="<?php echo $this->urlWithQs ?>" method="post" class="generic-form">
		<table class="table">
			<div class="col-md-7" >
				<div class="form-g inner-addon left-addon">
					<i class="fa fa-envelope">&nbsp;</i>
					<input class="form-control" type="text" name="email" id="email" placeholder="New E-Email Address" />
				</div>
				<p><strong>Note:</strong> <?php echo htmlspecialchars(Flux::message('EmailChangeInputNote')) ?></p>		
			</div>
			<div class="col-md-7" >
				<input class="form-btn" type="submit" value="<?php echo htmlspecialchars(Flux::message('EmailChangeButton')) ?>" />
			</div>
		</table>
	</form>
</div>
