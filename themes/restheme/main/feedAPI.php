<?php

class RSSFeed {

	protected $xml = '<?xml>';
	protected $entries = array();
	protected $feed_link = '';
	protected $cacheEnabled = TRUE; // the caching system is on
	protected $cacheFile = "cache/%s.xml"; // cache file - folder must have write permissions
	protected $cacheRelativePath = ""; // if set, then add a trailing / (slash)
	protected $cacheLifetime = 600; // (10min = 60sec * 10)
	protected $timezoneDifference = 36000; // (60sec * 60min * 10h) // set timezone difference

	public function setCachingOptions($cacheFile,$cacheLifetime) {
		$this->cacheFile = $cacheFile;
		$this->cacheLifetime = $cacheLifetime;
	}

	public function setCachingDirectory($cacheRelativePath) {
		$this->cacheRelativePath = $cacheRelativePath;
	}

	public function enableCaching($caching=TRUE) {
		$this->cacheEnabled = $caching;
	}

	public function addFeed($feed, $shortName, $limit=10, $sort_ascendent=FALSE) {
		$this->addFeedSource($feed, $shortName, $limit, $sort_ascendent);
		return $this->processCurrentFeedEntries($limit, $sort_ascendent, $shortName);
	}

	private function addFeedSource($feed, $shortName, $limit, $sort_ascendent) {
		try {
			$cacheFile = strtolower(sprintf($this->cacheFile, $shortName));

			// load cache if it exists and it hasn't expired
			if($this->cacheEnabled && file_exists($this->cacheRelativePath.$cacheFile)) {
				$cacheTime = filemtime($this->cacheRelativePath.$cacheFile);
				if($cacheTime > time() - $this->cacheLifetime) {
					$this->xml = file_get_contents($this->cacheRelativePath.$cacheFile);
					return;
				}
			}
			$ch = curl_init($feed);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			
			$this->xml = curl_exec($ch);
			$responseInfo = curl_getinfo($ch);
			$httpCode = $responseInfo['http_code'];
			curl_close($ch);
			if ($httpCode == 200) {
				if ($this->cacheEnabled) {
					// cache the results
					file_put_contents($this->cacheRelativePath.$cacheFile, $this->xml);
				}
			} else if ($httpCode == 404) {
				return;
			} else {
				if ($this->cacheEnabled && file_exists($this->cacheRelativePath.$cacheFile)) {
					$this->xml = file_get_contents($this->cacheRelativePath.$cacheFile);
				}
			}
		}
		catch (Exception $e) { }
	}

	public function time_since($original) {
		$chunks = array(
			array(31536000 , 'year'), // 60 * 60 * 24 * 365
			array(2592000 , 'month'), // 60 * 60 * 24 * 30
			array(604800, 'week'), // 60 * 60 * 24 * 7
			array(86400 , 'day'), // 60 * 60 * 24
			array(3600 , 'hour'), // 60 * 60
			array(60 , 'min'),
		);
		$today = time();
		$since = $today - $original + $this->timezoneDifference;

		// $j saves performing the count function each time around the loop
		for ($i = 0, $j = count($chunks); $i < $j; $i++) {
			$seconds = $chunks[$i][0];
			$name = $chunks[$i][1];
			// finding the biggest chunk (if the chunk fits, break)
			if (($count = floor($since / $seconds)) != 0) break;
		}
		$print = ($count == 1) ? "$count {$name} ago" : "$count {$name}s ago";
		return $print;
	}

	public function getFeedEntries() {
		$result = array(
			'link' => $this->feed_link,
			'items' => $this->entries,
		);
		return $result;
	}

	private function processCurrentFeedEntries($limit=10, $sort_ascendent=FALSE, $shortName='') {
		$result = simplexml_load_string($this->xml);
		if ( !$result ) return FALSE;

		$link = $result->xpath("channel/link[1]");
		if ( isset($link) ) $link = $link[0];
		if ($limit==-1) {
			$items = $result->xpath("channel//item");
		} else {
			$items = $result->xpath("channel//item[position()<=".$limit."]");
		}

		foreach($items as $item) {
			$item->updated = $this->time_since((int) strtotime($item->pubDate));
			$item->description = strip_tags($item->description);
			if ($shortName) $item->feedname = $shortName;
			array_push($this->entries, $item);
		}
		
		// Sort feed entries by pubDate (ascending)
		if ($sort_ascendent) {
			usort($this->entries, create_function('$x, $y', 'return strtotime($x->pubDate) - strtotime($y->pubDate);'));
		} else {
			usort($this->entries, create_function('$x, $y', 'return strtotime($y->pubDate) - strtotime($x->pubDate);'));
		}
		$this->feed_link = $link;
		return $this->getFeedEntries();
	}
}