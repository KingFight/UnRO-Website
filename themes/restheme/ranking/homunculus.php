<?php if (!defined('FLUX_ROOT')) exit; ?>
<h2>Homunculus Ranking</h2>
<h3>
	Top <?php echo number_format($limit=(int)Flux::config('HomunRankingLimit')) ?> Homunculi
	<?php if (!is_null($homunClass)): ?>
	(<?php echo htmlspecialchars($className=$this->homunClassText($homunClass)) ?>)
	<?php endif ?>
	on <?php echo htmlspecialchars($server->serverName) ?>
</h3>
<?php if ($homuns): ?>
<form action="" method="get" class="search-form2">
	<?php echo $this->moduleActionFormInputs('ranking', 'homun') ?>
	<p>
		<label for="homunclass">Filter by class:</label>
		<select name="homunclass" id="homunclass">
			<option value=""<?php if (is_null($homunClass)) echo 'selected="selected"' ?>>All</option>
		<?php foreach ($classes as $homunClassIndex => $homunClassName): ?>
			<option value="<?php echo $homunClassIndex ?>"
				<?php if (!is_null($homunClass) && $homunClass == $homunClassIndex) echo ' selected="selected"' ?>>
				<?php echo htmlspecialchars($homunClassName) ?>
			</option>
		<?php endforeach ?>
		</select>
		
		<input type="submit" value="Filter" />
		<input type="button" value="Reset" onclick="reload()" />
	</p>
</form>
<table id="restable" class="table table-striped">
	<tr id="hrow">
		<th>Rank</th>
		<th>Homunculus</th>
		<th>Owner</th>
		<th>Intimacy</th>
		<th>Level</th>
		<th>Experience</th>
	</tr>
	<?php $topRankType = !is_null($homunClass) ? $className : 'homunculus' ?>
	<?php for ($i = 0; $i < $limit; ++$i): ?>
	<tr<?php if (!isset($homuns[$i])) echo ' class="empty-row"'; if ($i === 0) echo ' class="top-ranked" title="<strong>'.htmlspecialchars($homuns[$i]->homun_name).'</strong> is the top ranked '.$topRankType.'!"' ?>>
		<td align="right" tableHeadData='Rank'><?php echo number_format($i + 1) ?></td>
		<?php if (isset($homuns[$i])): ?>
		<td tableHeadData='Homunculus'><strong><?php echo htmlspecialchars($homuns[$i]->homun_name) ?></strong></td>
		<td tableHeadData='Owner'><strong>
			<?php if ($auth->actionAllowed('character', 'view') && $auth->allowedToViewCharacter): ?>
				<?php echo $this->linkToCharacter($homuns[$i]->owner, $homuns[$i]->owner_name) ?>
			<?php else: ?>
				<?php echo htmlspecialchars($homuns[$i]->owner_name) ?>
			<?php endif ?>
		</strong></td>
		<td tableHeadData='Intimacy'><?php echo number_format($homuns[$i]->intimacy) ?></td>
		<td tableHeadData='Level'><?php echo number_format($homuns[$i]->level) ?></td>
		<td tableHeadData='Experience'><?php echo number_format($homuns[$i]->exp) ?></td>
		<?php else: ?>
		<td colspan="8" tableHeadData='None'></td>
		<?php endif ?>
	</tr>
	<?php endfor ?>
</table>
<?php else: ?>
<p>There are no homunculi. <a href="javascript:history.go(-1)">Go back</a>.</p>
<?php endif ?>
