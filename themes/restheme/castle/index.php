<?php if (!defined('FLUX_ROOT')) exit; ?>
<h2>Castles</h2>
<p>This page shows what castles are activated and which guilds own them.</p>
<?php if ($castles): ?>
<table id="restable" class="table table-striped">
	<tr id="hrow">
		<th>Castle ID</th>
		<th>Castle</th>
		<th colspan="2">Guild</th>
	</tr>
	<?php foreach ($castles as $castle): ?>
		<tr>
			<td tableHeadData='Castle ID'><?php echo htmlspecialchars($castle->castle_id) ?></td>
			<td tableHeadData='Castle'><?php echo htmlspecialchars($castleNames[$castle->castle_id]) ?></td>
			<?php if ($castle->guild_name): ?>
				<?php if ($castle->emblem_len): ?>
					<td width="24" tableHeadData='Guild'><img src="<?php echo $this->emblem($castle->guild_id) ?>" /></td>
					<td tableHeadData='Guild'>
						<?php if ($auth->actionAllowed('guild', 'view') && $auth->allowedToViewGuild): ?>
							<?php echo $this->linkToGuild($castle->guild_id, $castle->guild_name) ?>
						<?php else: ?>
							<?php echo htmlspecialchars($castle->guild_name) ?>
						<?php endif ?>
					</td>
				<?php else: ?>
					<td colspan="2" tableHeadData='GuilD'><?php echo htmlspecialchars($castle->guild_name) ?></td>
				<?php endif ?>
			<?php else: ?>
				<td colspan="2" tableHeadData='Guild'><span class="not-applicable"><?php echo htmlspecialchars(Flux::message('NoneLabel')) ?></span></td>
			<?php endif ?>
		</tr>
	<?php endforeach ?>
</table>
<?php else: ?>
<p>No castles found. <a href="javascript:history.go(-1)">Go back</a>.</p>
<?php endif ?>
